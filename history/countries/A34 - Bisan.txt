government = monarchy
add_government_reform = feudalism_reform
government_rank = 1
primary_culture = wexonard
religion = regent_court
technology_group = tech_cannorian
capital = 300 # Bisan
national_focus = DIP

1000.1.1 = { set_country_flag = mage_organization_magisterium_flag }

1413.3.3 = {
	monarch = {
		name = "Harold V"
		dynasty = "of Bisan"
		birth_date = 1405.2.6
		adm = 0
		dip = 3
		mil = 2
	}
}

1422.1.1 = { set_country_flag = lilac_wars_moon_party }