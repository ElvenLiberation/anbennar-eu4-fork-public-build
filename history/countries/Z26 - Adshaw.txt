government = monarchy
add_government_reform = feudalism_reform
government_rank = 1
primary_culture = blue_reachman
religion = regent_court
technology_group = tech_cannorian
capital = 731
national_focus = DIP

1000.1.1 = { set_country_flag = mage_organization_decentralized_flag }

1440.10.7 = {
	monarch = {
		name = "Rycan IV"
		dynasty = "Adshaw"
		birth_date = 1401.10.1
		adm = 2
		dip = 3
		mil = 2
	}
}