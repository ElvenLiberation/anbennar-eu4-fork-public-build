government = monarchy
add_government_reform = feudalism_reform
government_rank = 2
primary_culture = gawedi
religion = regent_court
technology_group = tech_cannorian
capital = 221
national_focus = DIP
historical_rival = A01 #Lorent
historical_rival = A30 #Wex

1000.1.1 = { set_country_flag = mage_organization_centralized_flag }	#Cobalt Circle

1435.3.17 = {
	monarch = {
		name = "Welyam III"
		dynasty = "Gerwick"
		birth_date = 1427.10.3
		adm = 1
		dip = 2
		mil = 2
	}
	add_ruler_personality = babbling_buffoon_personality
}