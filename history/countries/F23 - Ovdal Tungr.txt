government = republic
add_government_reform = noble_elite_reform
government_rank = 1
primary_culture = copper_dwarf
religion = ancestor_worship
technology_group = tech_dwarven
capital = 526
historical_rival = F14 #Mountainhuggers
historical_rival = F18 #Re'uyel
historical_friend = F17

1000.1.1 = { set_country_flag = mage_organization_centralized_flag }
