# No previous file for Sidhalda
owner = Z11
controller = Z11
add_core = Z11
culture = east_dalr
religion = skaldhyrric_faith

hre = no

base_tax = 3
base_production = 3
base_manpower = 3

trade_goods = fur

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_gnomish
discovered_by = tech_giantkind