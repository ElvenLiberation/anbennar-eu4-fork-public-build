# No previous file for Yedishkul
owner = H17
controller = H17
add_core = H17
religion = eordellon
capital = ""

hre = no

base_tax = 2
base_production = 2
base_manpower = 2

trade_goods = precursor_relics
add_permanent_province_modifier = {
	name = eordellonian_expedition_site
	duration = -1
}

culture = snecboth