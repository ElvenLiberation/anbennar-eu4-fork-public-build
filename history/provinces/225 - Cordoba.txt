#225 - Cordoba |

owner = A13
controller = A13
add_core = A13
add_core = A50
culture = old_alenic
religion = regent_court

hre = no

base_tax = 4
base_production = 4
base_manpower = 2

trade_goods = fur
center_of_trade = 1

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold
discovered_by = tech_orcish

