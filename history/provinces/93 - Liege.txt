#93 - Liege | 

owner = A21
controller = A21
add_core = A21
culture = moon_elf
religion = elven_forebears

hre = no

base_tax = 10
base_production = 9
base_manpower = 4

trade_goods = paper
capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish