#214 - Aragon | 

owner = A23
controller = A23
add_core = A23
culture = vertesker
religion = regent_court

hre = yes

base_tax = 1
base_production = 2
base_manpower = 1

trade_goods = salt

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold

