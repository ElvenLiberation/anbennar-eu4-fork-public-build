# No previous file for Torbjoldhavn
owner = Z08
controller = Z08
add_core = Z08
culture = west_dalr
religion = skaldhyrric_faith

hre = no

base_tax = 4
base_production = 5
base_manpower = 3

trade_goods = livestock

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_gnomish
discovered_by = tech_giantkind
