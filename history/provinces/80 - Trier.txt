owner = A15
controller = A15
add_core = A15
culture = high_lorentish
religion = regent_court
hre = no
base_tax = 5
base_production = 4
trade_goods = cloth
base_manpower = 2
capital = ""
is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish

