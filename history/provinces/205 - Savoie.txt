#205 - Savoie | 

owner = A12
controller = A12
add_core = A12
culture = imperial_halfling
religion = regent_court

hre = yes

base_tax = 3
base_production = 3
base_manpower = 3

trade_goods = livestock

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold

