# No previous file for Jotunskol
owner = Z09
controller = Z09
add_core = Z09
culture = olavish
religion = skaldhyrric_faith

hre = no

base_tax = 1
base_production = 1
base_manpower = 1

trade_goods = naval_supplies

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_gnomish
discovered_by = tech_giantkind