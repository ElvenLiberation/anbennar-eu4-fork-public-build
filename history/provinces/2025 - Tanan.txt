# No previous file for Tanan
owner = H01
controller = H01
add_core = H01
culture = selphereg
religion = eordellon
capital = "Tanan"

hre = no

base_tax = 3
base_production = 2
base_manpower = 1

trade_goods = silk

native_size = 14
native_ferocity = 6
native_hostileness = 6