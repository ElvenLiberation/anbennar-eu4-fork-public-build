# No previous file for Malacca
owner = F43
controller = F43
add_core = F43
culture = brasanni
religion = bulwari_sun_cult

hre = no

base_tax = 6
base_production = 5
base_manpower = 3

trade_goods = spices
center_of_trade = 1

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari