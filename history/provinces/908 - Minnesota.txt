#908 - Minnesota

owner = A89
controller = A89
add_core = A89
culture = crownsman
religion = regent_court

hre = yes

base_tax = 8
base_production = 6
base_manpower = 4

trade_goods = cloth

capital = ""

is_city = yes
fort_15th = yes 

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish

add_permanent_province_modifier = {
	name = ainway_river_toll
	duration = -1
}