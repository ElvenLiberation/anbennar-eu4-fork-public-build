
namespace = flavor_jaherkand

#Dynasty Naming
country_event = {
	id = flavor_jaherkand.1
	title = dynasty_setup.1.t
	desc = dynasty_setup.1.d
	picture = {
		trigger = {
			NOT = { has_dlc = "Rights of Man" }
		}
		picture = COURT_eventPicture
	}
	picture = {
		trigger = {
			has_dlc = "Rights of Man"
		}
		picture = ROYAL_COUPLE_FUTURE_eventPicture
	}
	
	fire_only_once = yes
	is_triggered_only = yes
	
	trigger = {
		tag = H67
	}
	
	#New Dynasty Name after country
	option = {		
		name = "flavor_jaherkand.1.a"
		ai_chance = { factor = 50 }	
		define_heir = {
			dynasty = "Kandzuir"
			age = 60
			male = yes
			adm = 2
			dip = 2
			mil = 2
			hide_skills = yes
		}
	}
}