#Country Name: Please see filename.

graphical_culture = southamericagfx

color = { 216  85  67 }

revolutionary_colors = { 216  85  67 }

historical_idea_groups = {
	economic_ideas
	offensive_ideas
	influence_ideas
	defensive_ideas	
	administrative_ideas	
	trade_ideas
	quality_ideas
	innovativeness_ideas
}

historical_units = {
	native_indian_archer
	native_indian_tribal_warfare
	algonkin_tomahawk_charge
	native_indian_horsemen
	huron_arquebusier
	commanche_swarm
	native_indian_mountain_warfare
	american_western_franchise_warfare
}

monarch_names = {

	#Generic Kalavendhi Names
	"Adradar #0" = 50
	"Aldamothi #0" = 50
	"Aldatam #0" = 50
	"Alvarathik #0" = 50
	"Andrellatam #0" = 50
	"Aranrudu #0" = 50
	"Ardpanatam #0" = 50
	"Ardpanth #0" = 50
	"Artoratam #0" = 50
	"Calindash #0" = 50
	"Camnan #0" = 50
	"Camnand #0" = 50
	"Camnavna #0" = 50
	"Darandar #0" = 50
	"Daratam #0" = 50
	"Denariatam #0" = 50
	"Dorenpan #0" = 50
	"Ebenanth #0" = 50
	"Eboranbu #0" = 50
	"Elashok #0" = 50
	"Eletam #0" = 50
	"Elrandar #0" = 50
	"Elrathikeyan #0" = 50
	"Eranbu #0" = 50
	"Erandar #0" = 50
	"Erennan #0" = 50
	"Erlander #0" = 50
	"Ervram #0" = 50
	"Ervramesh #0" = 50
	"Evindash #0" = 50
	"Filioth #0" = 50
	"Finoratam #0" = 50
	"Galinpan #0" = 50
	"Geledish #0" = 50
	"Gelmondar #0" = 50
	"Jahlonand #0" = 50
	"Jahlorali #0" = 50
	"Jalenesh #0" = 50
	"Jalinthik #0" = 50
	"Munathik #0" = 50
	"Olordar #0" = 50
	"Pelozhil #0" = 50
	"Seronpan #0" = 50
	"Seronraha #0" = 50
	"Seronzhil #0" = 50
	"Tailananth #0" = 50
	"Tailarudu #0" = 50
	"Tailazhil #0" = 50
	"Teshalashothi #0" = 50
	"Teshalropan #0" = 50
	"Tesharanthi #0" = 50
	"Teshelapan #0" = 50
	"Teshelazhil #0" = 50
	"Thaladish #0" = 50
	"Thanand #0" = 50
	"Thelriatam #0" = 50
	"Thirendep #0" = 50
	"Thirendish #0" = 50
	"Threzhil #0" = 50
	"Triander #0" = 50
	"Ultazhil #0" = 50
	"Uratand #0" = 50
	"Varamand #0" = 50
	"Varamesh #0" = 50
	"Varamzhil #0" = 50
	"Varanarma #0" = 50
	"Varanash #0" = 50
	"Varelrudu #0" = 50
	"Vatesheran #0" = 50
	"Vindar #0" = 50
	
	"Aladewarya #0" = -10
	"Alanarothi #0" = -10
	"Alarizhila #0" = -10
	"Alerchana #0" = -10
	"Amarchana #0" = -10
	"Ariamarai #0" = -10
	"Arichana #0" = -10
	"Calsithara #0" = -10
	"Camdathna #0" = -10
	"Celachana #0" = -10
	"Devenasha #0" = -10
	"Eburiarthi #0" = -10
	"Elarizhila #0" = -10
	"Elechana #0" = -10
	"Erlanartha #0" = -10
	"Erumarai #0" = -10
	"Filinarothi #0" = -10
	"Galichana #0" = -10
	"Imatema #0" = -10
	"Isehrivana #0" = -10
	"Isermathi #0" = -10
	"Iserothi #0" = -10
	"Istralendra #0" = -10
	"Ivranarha #0" = -10
	"Ivranarhi #0" = -10
	"Jexiarthi #0" = -10
	"Ladrarha #0" = -10
	"Lanahasha #0" = -10
	"Lanodana #0" = -10
	"Lelizhila #0" = -10
	"Leslarasha #0" = -10
	"Liandarha #0" = -10
	"Merialavu #0" = -10
	"Mihtratama #0" = -10
	"Narazhila #0" = -10
	"Nathachana #0" = -10
	"Seluslavi #0" = -10
	"Seronchana #0" = -10
	"Shariava #0" = -10
	"Sharinarha #0" = -10
	"Sherala #0" = -10
	"Shiarthi #0" = -10
	"Taneliata #0" = -10
	"Thaliarha #0" = -10
	"Valamathi #0" = -10
	"Varilata #0" = -10
	"Varimarai #0" = -10
	"Varinzhila #0" = -10
	"Vehachana #0" = -10
	"Veharathna #0" = -10
	"Vesimathi #0" = -10
	"Zalerimathi #0" = -10
	
}

leader_names = {

	Mudaliar
	Rajapopalachari
	Sastri
	Dalawa
	Thondaiman
	Saluva
	Saraf
	Nayak
	Chetty
	Senji
	Bahadur
	Khader
	Reddy
	Mudaliar
	Rajapopalachari
	Thondaiman
	Saluva
	Chetty
	Sastri
	
}

ship_names = {
	Amirtha Amman Arul Chellam Devi
	Durga Gayatri Iniya Kaveri Lakshmi
	Madhu Meenakshi Nalini Palar Parvati
	Pennar Ponnaiyar Ponni Radha Ratri
	Sarasvati Sita Tamarai Vaigai
}	

army_names = {
	"Kalavendhi Army" "Army of $PROVINCE$"
}

fleet_names = {
	"Kalavendhi Fleet"
}