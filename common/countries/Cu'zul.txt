#Country Name: Please see filename.

graphical_culture = northamericagfx

color = { 117  135  70 }

revolutionary_colors = { 117  135  70 }

historical_idea_groups = {
	economic_ideas
	offensive_ideas
	influence_ideas
	defensive_ideas	
	administrative_ideas	
	trade_ideas
	quality_ideas
	innovativeness_ideas
}

historical_units = {
	native_indian_archer
	native_indian_tribal_warfare
	algonkin_tomahawk_charge
	native_indian_horsemen
	huron_arquebusier
	commanche_swarm
	native_indian_mountain_warfare
	american_western_franchise_warfare
}

monarch_names = {

	#Generic Aelantiri Names
	"Adra'hai #0" = 1
	"Adra'haio #0" = 1
	"Alarenu #0" = 1
	"Ald'ar #0" = 1
	"Ald'aro #0" = 1
	"Andrel'on #0" = 1
	"Apel #0" = 1
	"Aran #0" = 1
	"Ardo #0" = 1
	"Ardo'i #0" = 1
	"Arna'do #0" = 1
	"Arto'i #0" = 1
	"Artoi #0" = 1
	"Bake'renne #0" = 1
	"Benedo #0" = 1
	"Camno #0" = 1
	"Camno'ari #0" = 1
	"Camno'arin #0" = 1
	"Caro #0" = 1
	"Caro'do #0" = 1
	"Cela'dole #0" = 1
	"Dorasto'entu #0" = 1
	"Doren'do #0" = 1
	"Ebran #0" = 1
	"Ebran'do #0" = 1
	"Eebo #0" = 1
	"Ele'il #0" = 1
	"Elr #0" = 1
	"Elro #0" = 1
	"Elthe'das #0" = 1
	"Elthe'dor #0" = 1
	"Elthe'thar #0" = 1
	"Err'ir #0" = 1
	"Err'lasa #0" = 1
	"Evn'do #0" = 1
	"Evn'do #0" = 1
	"Evn'enn #0" = 1
	"Fin'o #0" = 1
	"Galendo #0" = 1
	"Galenel #0" = 1
	"Geln #0" = 1
	"Gelr #0" = 1
	"Jarre #0" = 1
	"Jarreh #0" = 1
	"Kaland #0" = 1
	"Kalandu #0" = 1
	"Kalin'do #0" = 1
	"Kel'do #0" = 1
	"Kel'doro #0" = 1
	"Monda #0" = 1
	"Nestetu #0" = 1
	"Olo #0" = 1
	"Olo'don #0" = 1
	"Olodo #0" = 1
	"Pele'na #0" = 1
	"Pelo #0" = 1
	"Seron'ao #0" = 1
	"Seron'ar #0" = 1
	"Seron'do #0" = 1
	"Seron'o #0" = 1
	"Talai #0" = 1
	"Talor #0" = 1
	"Teler'osan #0" = 1
	"Teler'osandai #0" = 1
	"Teler'osandar #0" = 1
	"Threthensdo #0" = 1
	"Tiren #0" = 1
	"Tiren'do #0" = 1
	"Tiren'hai #0" = 1
	"Ultar #0" = 1
	"Uron #0" = 1
	"Vanai #0" = 1
	"Vara'male #0" = 1
	"Vara'mall #0" = 1
	"Vara'malleno #0" = 1
	"Vara'mar #0" = 1
	"Varel'do #0" = 1
	"Varn #0" = 1
	
	"Ala'a #0" = -10
	"Alar'a #0" = -10
	"Alera'ar #0" = -10
	"Alra'e #0" = -10
	"Arat'a #0" = -10
	"Arat'e #0" = -10
	"Arat'o #0" = -10
	"Cala'a #0" = -10
	"Cel'ada #0" = -10
	"Cema'denn #0" = -10
	"Deve #0" = -10
	"Deve'ne #0" = -10
	"Eb'den #0" = -10
	"Eestra #0" = -10
	"El'den #0" = -10
	"Ela'a #0" = -10
	"Ere'denn #0" = -10
	"Fel'asa #0" = -10
	"Galenel #0" = -10
	"Imar'el #0" = -10
	"Isere #0" = -10
	"Isere'el #0" = -10
	"Ishe'e #0" = -10
	"Ivran'a #0" = -10
	"Ivran'e #0" = -10
	"Jexa'al #0" = -10
	"Laden'al #0" = -10
	"Ladena #0" = -10
	"Lana'ar #0" = -10
	"Lana'le #0" = -10
	"Lelia'ar #0" = -10
	"Leyan'd #0" = -10
	"Leyan'de #0" = -10
	"Meyal'e #0" = -10
	"Mith'le #0" = -10
	"Nara #0" = -10
	"Nathel'ar #0" = -10
	"Pana #0" = -10
	"Rel'asa #0" = -10
	"Saeran'le #0" = -10
	"Selu'osa #0" = -10
	"Sero'denn #0" = -10
	"Shar'a #0" = -10
	"Sharin'ar #0" = -10
	"Sharin'el #0" = -10
	"Sheva'le #0" = -10
	"Tene'a #0" = -10
	"Tha'ada #0" = -10
	"Var'i #0" = -10
	"Var'ie #0" = -10
	"Veha'i #0" = -10
	"Vehari #0" = -10
	"Zale'ar #0" = -10
	
}

leader_names = {

	#Just random native stuff to be honest
	Awegen Ahaouet
	Canajoharie Canastigaone Canienga Caughnawaga Cahunghage Canowaroghere
	Deseroken Dayoitgao Deonundagae Deyodeshot Deyohnegano Deyonongdadagana
	Gweugwehono Gandasetaigon Ganogeh Gayagaanhe Gewauga Goiogouen Gadoquat Gannentaha
	Hostayuntwa
	Kawauka Kente Kanagaro Kowogoconnughariegugharie Kanadaseagea Kanatakowa
	Neodakheat Nowadaga Nundawaono
	Onekagoncka Onoalagona Oquaga Osquake Onayotekaono Oriska Ossewingo 
	Skannayutenate Saratoga Schaunactada Schoharie
	Teatontaloga Tewanondadon Tionnontoguen Tegasoke Teseroken Tetosweken 
	Yoroonwago
	
	Aisikstukiks
	Apikaiyikis
	Emitahpahksaiyiks
	Motahtosiks
	Puhksinahmahyiks
	Saiyiks
	Siksinokaks
	Tsiniktsistsoyiks
	Ahkaisumiks
	Ahkaipokaks
	Ahkotahshiks
	Anepo
	Apikaiyiks
	Aputosikainah
	Inuhksoyistamiks
	Isissokasimiks
	Istsikaihan
	Mameoya
	Nitikskiks
	Saksinahmahyiks
	Sikahpuniks
	Sikinokaks
	Ahahpitape
	Ahkaiyikokakiniks
	Apikaiyiks
	Esksinaitupiks
	Inuksikahpowaiks
	Inuksiks
	Ipoksimaiks
	Kahmitaiks
	Kiyis
	Kutaiimiks
	Kutaisotsiman
	Miahwahpitsiks
	Miawkinaiyiks
	Mokumiks
	Motahtosiks
	Motwainaiks
	Nitakoskitsipupiks
	Niwayiks
	Nitikskiks
	Nitotsiksistaniks
	Sikokitsimiks
	Sikopoksimaiks
	Sikutsipumaiks
	Susksoyiks
	Tsiniksistsoyiks
	
}

ship_names = {
	Gorogoco
	Cucuacu
}	

army_names = {
	"$PROVINCE$ Zulcua" "Zul Zulcua"
}

fleet_names = {
	"$PROVINCE$ Zulmipichimipi" "Zul Zulmipichimipi"
}