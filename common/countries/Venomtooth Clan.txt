#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 114  140  0 }

revolutionary_colors = { 114  140  0 }

historical_idea_groups = {
	offensive_ideas
	expansion_ideas
	exploration_ideas
	quantity_ideas
	quality_ideas	
	defensive_ideas
	economic_ideas	
	innovativeness_ideas
}

historical_units = {
	western_medieval_infantry
	chevauchee
	western_men_at_arms
	swiss_landsknechten
	dutch_maurician
	french_caracolle
	anglofrench_line
	french_dragoon
	french_bluecoat
	french_cuirassier
	french_impulse
	mixed_order_infantry
	open_order_cavalry
	napoleonic_square
	napoleonic_lancers
}

monarch_names = {

		#Generic Orc Name List
		"Ad�l #0" = 10
		"Amug #0" = 10
		"Azukor #0" = 10
		"Bagga #0" = 10
		"Bakbur #0" = 10
		"Bhagzud #0" = 10
		"Bolg #0" = 10
		"Borgu #0" = 10
		"Bork #0" = 10
		"Bral #0" = 10
		"Bras�r #0" = 10
		"Burggob #0" = 10
		"B�rog #0" = 10
		"B�bol #0" = 10
		"B�th #0" = 10
		"Conakk #0" = 10
		"Dhog #0" = 10
		"Dramm #0" = 10
		"Drazad #0" = 10
		"Drok #0" = 10
		"Dub #0" = 10
		"Dugz #0" = 10
		"Durg #0" = 10
		"D�sh #0" = 10
		"Flak #0" = 10
		"Flogg #0" = 10
		"Fubar #0" = 10
		"Gakk #0" = 10
		"Garl #0" = 10
		"Garm #0" = 10
		"Ghad #0" = 10
		"Ghash #0" = 10
		"Ghudd #0" = 10
		"Ghudrag #0" = 10
		"Ghur�d #0" = 10
		"Gh�m #0" = 10
		"Gluk #0" = 10
		"Gokk #0" = 10
		"Golm #0" = 10
		"Grisha #0" = 10
		"Grom #0" = 10
		"Gruk #0" = 10
		"Grumger #0" = 10
		"Gub� #0" = 10
		"Gukk #0" = 10
		"Gund #0" = 10
		"Hoglik #0" = 10
		"Hork #0" = 10
		"Horza #0" = 10
		"Hoshgrish #0" = 10
		"Hura #0" = 10
		"Ikom #0" = 10
		"Ishmoz #0" = 10
		"Kargath #0" = 10
		"Khagor #0" = 10
		"Khrosh #0" = 10
		"Kilrogg #0" = 10
		"Korgus #0" = 10
		"Koth #0" = 10
		"Krakhorn #0" = 10
		"Krimp #0" = 10
		"Kr�k #0" = 10
		"Kuga #0" = 10
		"Kurd #0" = 10
		"Larluk #0" = 10
		"Lorm #0" = 10
		"Luga #0" = 10
		"Lurb #0" = 10
		"Mabug #0" = 10
		"Maknag #0" = 10
		"Maku #0" = 10
		"Mogg #0" = 10
		"Mogru #0" = 10
		"Moz� #0" = 10
		"M�g #0" = 10
		"M�l #0" = 10
		"Muz� #0" = 10
		"Norsko #0" = 10
		"Nor�k #0" = 10
		"N�kra #0" = 10
		"N�rug #0" = 10
		"N�zu #0" = 10
		"Ogal #0" = 10
		"Ogb�r #0" = 10
		"Ogg #0" = 10
		"Okegg #0" = 10
		"Olrok #0" = 10
		"Orgrim #0" = 10
		"Orlog #0" = 10
		"Orok #0" = 10
		"Orok�g #0" = 10
		"Orthog #0" = 10
		"Prak #0" = 10
		"Pug #0" = 10
		"Pushkrimp #0" = 10
		"Ragdug #0" = 10
		"Rakk #0" = 10
		"Rash #0" = 10
		"Ratak #0" = 10
		"Regar #0" = 10
		"Ronk #0" = 10
		"Rug #0" = 10
		"Sam�r #0" = 10
		"Shador #0" = 10
		"Shaka #0" = 10
		"Sh�g #0" = 10
		"Skak #0" = 10
		"Skoth #0" = 10
		"Skun #0" = 10
		"Snafu #0" = 10
		"Stakug� #0" = 10
		"Takra #0" = 10
		"Tarz #0" = 10
		"Thrak #0" = 10
		"Thr�l #0" = 10
		"Tugog #0" = 10
		"Tuhorn #0" = 10
		"T�ka #0" = 10
		"Udd�g #0" = 10
		"Ugakuga #0" = 10
		"Ugol #0" = 10
		"Ukbuk #0" = 10
		"Urim #0" = 10
		"Urimguk #0" = 10
		"Ushak #0" = 10
		"Uz�l #0" = 10
		"Varbuk #0" = 10
		"Varok #0" = 10
		"Zathra #0" = 10
		"Zhorg #0" = 10
		"Zhuk #0" = 10
		"Zog #0" = 10
		"Zosh #0" = 10
		"Zugor #0" = 10
		"Zur #0" = 10
		"Z�b #0" = 10
		"Z�ka #0" = 10
		
		"Ahe #0" = -10
		"Ahza #0" = -10
		"Atu #0" = -10
		"Awa #0" = -10
		"A�dga #0" = -10
		"Bhiel #0" = -10
		"Borba #0" = -10
		"Bur� #0" = -10
		"Dhoulza #0" = -10
		"Etrega #0" = -10
		"Ghorza #0" = -10
		"Glasha #0" = -10
		"Grama #0" = -10
		"Grane #0" = -10
		"G�rona #0" = -10
		"Khaara #0" = -10
		"Lagba #0" = -10
		"Lamma #0" = -10
		"Lazga #0" = -10
		"Mewa #0" = -10
		"Muroga #0" = -10
		"Neevn�z #0" = -10
		"Ohdaka #0" = -10
		"Orbha #0" = -10
		"Or�ph #0" = -10
		"Ovga #0" = -10
		"Rerk�l #0" = -10
		"Rurti #0" = -10
		"Shel #0" = -10
		"Sheru #0" = -10
		"Tid� #0" = -10
		"T�tga #0" = -10
		"Ugl�im #0" = -10
		"Uroph #0" = -10
		"Vogaa #0" = -10
		"Voltga #0" = -10
		"Yagza #0" = -10
		"Zien #0" = -10
}

leader_names = {

	#Occupational Surnames
	Skullgrinder "Mountain-Eater" Ironskin Ironhide Deadhide Hammerfist Hammerspike Spikefist Swordfist "Dead-eye" Bloodspear Blackheart Manhunter Pureblood "Bright Eyes" "Eagle Eyes" "Half-Tongue" "Grog-Maker" Deathmonger
	Swordmaster Warborn Stoneskin Warmonger Rockskull Skullblade Skullhorn Ogreborn Deadeye Longtooth Pigsnout Regehammer Crueldrum Stonefist Angerfang Steelsword Warpsteel Deathforge Cravenkiller Hellspitter Rocksnarler
	Deadbreath Bitteraxe Shadowfeast Bloodfeast Redfeast Bloodscream Wildmarcher Brokenhammer Brokentusk "No-eyes" Silentblade Rageblood Rageblade Doomhammer Hellscream Blackhand Doomhammer Saurfang Bladefist Bloodreaver
	Brightflesh Grandguard Armorskin Armorhide Steelhide Metalhide Metaljaw Sharpears Halfblood Ragechain Chaoschain Chaosblade Flameseeker Fireblood Battlefire Thunderstorm Thunderblood Thunderfang Axemaul Shadowblood
	
	
	#Titles
	"'the Stout'" "'the Rash'" "'the Deadly'" "'the Master'" "'the Slaver'" "'the Warlord'" "'the Relentless'" "'the Bold'" "'the Warrior'" "'the Death-bringer'" "'the Handsome'" "'the Greedy'" "'the Dwarf-slayer'" "'the Tyrant'" "'the Unbreakable'" "'the Great'"
	"'the Defiler'" "'the Dead'" "'the Poker'" "'the Strangler'" "'the Twisted'" "'the Punisher'" "'the Devourer'" "'the Gravewalker'" "'the Crafty'" "'the Smart'" "'the Unwashed'" "'the Vile'" "'the Whisperer'" "'the Seer'" "'the Wicked'" "'the Brander'"
	"'the Large'" "'the Tall'" "'the Fat'" "'the Small'" "'the Strong'" "'the Clever'" "'the Crusher'" "'the Halfblooded"
}

ship_names = {
	#Generic Orcish
	Destruction Pillage Harness Power Axe Sword Tooth Nail Hammer Wrath Anger Dominion Domination Spike Severer "Severed Head" "Doom" Doomchosen Triump Tremendous Victory Conquest Courage Bravery Honour Honor "Great Dookan"
	Korgus "Sunken Dwarf" "Dead Dwarf" "Dwarf-mast" Skinflayer Skinhider "Skin-masts" Skullfeeder Skulltaker Defiance Devastation Formidable Impregnable Hunter War Warmarked Warchosen Terrible Terror Thunderer Tarrasque
	Banshee Harpy Goblintongue 
}

army_names = {
	"Venomtooth Army" "Orcish Horde" "Army $PROVINCE$"
} 

fleet_names = {
	"Venomtooth Fleet" "Orcish Fleet" "Dookan Squadron" "Black Squadron" "Green Squadron"
}