
lorent_lorentish_unification = {
	slot = 1
	generic = no
	ai = yes
	potential = {
		tag = A01
	}
	has_country_shield = yes
	
	lorent_restore_the_army = {
		icon = mission_assemble_an_army
		required_missions = { }
		position = 1
		
		trigger = {
			army_size_percentage = 0.8
			manpower_percentage = 0.4
			num_of_generals = 1
		}
		
		effect = {
			lower_bloodwine_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
		}
	}
	
	lorent_curtail_the_wine_lords = {
		icon = mission_have_two_subjects
		required_missions = { lorent_restore_the_army }
		position = 2

		provinces_to_highlight = {
			OR = {
				province_id = 114
				province_id = 108
				province_id = 79
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			114 = {
				country_or_non_sovereign_subject_holds = ROOT
			}
			108 = {
				country_or_non_sovereign_subject_holds = ROOT
			}
			79 = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		effect = {
			sorncost_vines_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			sornhills_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			add_casus_belli = {
				target = A09
				type = cb_vassalize_mission
			}
		}
	}
	
	lorent_conquer_sorncost = {
		icon = mission_cannons_firing
		required_missions = { lorent_curtail_the_wine_lords }
		position = 3

		provinces_to_highlight = {
			OR = {
				area = sorncost_vines_area
				area = sornhills_area
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			sorncost_vines_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			sornhills_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		effect = {
			deranne_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			add_casus_belli = {
				target = A02
				type = cb_vassalize_mission
			}
		}
	}
	lorent_conquer_deranne = {
		icon = mission_unite_home_region
		required_missions = { lorent_conquer_sorncost }
		
		
		provinces_to_highlight = {
			area = deranne_area
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			deranne_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		effect = {
			lencenor_region = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
		}
	}
	lorent_unification_of_lencenor = {
		icon = mission_build_up_to_force_limit
		required_missions = { lorent_conquer_deranne }
		
		provinces_to_highlight = {
			region = lencenor_region
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			lencenor_region = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		effect = {
			add_dip_power = 50
			add_adm_power = 50
			add_mil_power = 50
			add_country_modifier = {
				name = "hegemonic_ambition"
				duration = 9125 #25 years
			}
		}
	}
}

lorent_internal = {
	slot = 2
	generic = no
	ai = yes
	potential = {
		tag = A01
	}
	has_country_shield = yes
	
	lorent_recover_from_lilac_wars = {
		icon = mission_high_income
		required_missions = { }
		position = 1
		
		trigger = {
				stability = 2
			}
			
		effect = {
			add_country_modifier = {
				name = "building_spree"
				duration = 9125
			}
		}
	}
	
	lorent_reform_knights_of_the_rose = {
		icon = mission_rb_war_of_the_roses
		required_missions = { 
			lorent_recover_from_lilac_wars 
			lorent_restore_the_army
		}
		position = 2
		trigger = {
			num_of_cavalry = 10
			
		}

		effect = {
			add_country_modifier = {
				name = "lorent_reformed_knights_of_the_rose"
				duration = 5475
			}
			add_army_tradition = 30
		}
	}
	
	lorent_redglade_accord = {
		icon = mission_alliances
		required_missions = { lorent_reform_knights_of_the_rose }
		position = 3
		
		trigger = {
			A03 = {
				is_march = yes
				has_opinion = {
					who = ROOT
					value = 190
				}
			}
		}

		effect = {
			add_country_modifier = {
				name = "influential_diplomacy"
				duration = 3650 #10 years
			}
		}
	}
	
	lorent_renovating_lorentaine = {
		icon = mission_have_manufactories
		required_missions = { lorent_redglade_accord }
		position = 4
		
		provinces_to_highlight = {
			province_id = 67
		}
		
		trigger = {
			67 = {
				owned_by = ROOT
				has_building = marketplace
				has_building = temple
				has_building = workshop
			}
		}

		effect = {
			67 = {
				add_base_tax = 2
				add_base_production = 2
				add_base_manpower = 2
			}
			add_country_modifier = {
				name = "growth_of_capital"
				duration = 7300 #20 years
			}
		}
	}
	
	lorent_royal_mage_academy = {
		icon = mission_early_game_buildings
		required_missions = { lorent_renovating_lorentaine }
		position = 5
		
		provinces_to_highlight = {
			province_id = 67
		}
		
		trigger = {
			67 = {
				owned_by = ROOT
				has_building = university
			}
		}

		effect = {
			add_country_modifier = { 
				name = lorent_mages_of_the_ruby_order
				duration = -1  
			}
		}
	}
	
	lorent_the_ruby_crown = {
		icon = mission_empire
		required_missions = { lorent_royal_mage_academy }
		position = 6
		
		trigger = {
			adm = 6
			dip = 6
			mil = 6
			
			OR = {
				dynasty = "Siloriel"
				dynasty = "síl Rubenaire"
				dynasty = "síl Eilísin"
				dynasty = "síl Madelaire"
				dynasty = "síl Kyliande"
			}

		}

		effect = {
			country_event = {
				id = flavor_lorent.1
				days = 1
			}
		}
	}
	
}


lorent_colonial = {
	slot = 3
	generic = no
	ai = yes
	potential = {
		tag = A01
	}
	has_country_shield = yes
	
	lorent_endral_the_explorer = {
		icon = mission_establish_high_seas_navy
		required_missions = { lorent_redglade_accord }
		position = 4
		
		trigger = {
			has_idea_group = exploration_ideas
			has_idea = quest_for_the_new_world
		}
			
		effect = {
			define_explorer = {
				name = "Endral the Explorer"
				fire = 2
				shock = 2
				manuever = 6
				siege = 0
			}
		}
	}
	
	lorent_rediscovery_of_aelantir = {
		icon = mission_sea_battles
		required_missions = { lorent_endral_the_explorer }
		position = 5
		trigger = {
			custom_trigger_tooltip = {
				tooltip = aelantir_discovery.tooltip
				colonial_endralliande = {
					has_discovered = ROOT
				}
			}
		}

		effect = {
			add_country_modifier = {
				name = "colonial_enthusiasm"
				duration = 7300
			}
		}
	}
	
	lorent_colonize_endralliande = {
		icon = mission_dominate_home_trade_node
		required_missions = { lorent_rediscovery_of_aelantir }
		position = 6
		provinces_to_highlight = {
			colonial_region = colonial_endralliande
			NOT = { country_or_non_sovereign_subject_holds = ROOT }
			has_discovered = yes
		}
		
		trigger = {
			custom_trigger_tooltip = {
				tooltip = miss_colonize_endralliande
				num_of_provinces_owned_or_owned_by_non_sovereign_subjects_with = {
					value = 1
					colonial_region = colonial_endralliande
					is_city = yes
				}
			}
		}
		
		effect = {
			add_prestige = 15
			add_country_modifier = {
				name = "the_unknown_frontier"
				duration = 5475
			}
		}
	}
}


lorent_small_country = {
	slot = 4
	generic = no
	ai = yes
	potential = {
		tag = A01
	}
	has_country_shield = yes
	
	lorent_wars_of_dominion = {
		icon = mission_unite_home_region
		required_missions = { }
		#position = 1
		
		trigger = {
				if = {
					limit = {
						exists = A13
					}
					war_with = A13
			}
		}
		
		effect = {
			small_country_region = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			add_country_modifier = {
				name = "army_enthusiasm"
				duration = 7300
			}
		}
	}
	
	lorent_halfling_suzerainty = {
		icon = mission_monarch_in_throne_room
		required_missions = { lorent_wars_of_dominion }
		#position = 2
		
		provinces_to_highlight = {
			region = small_country_region
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		
		trigger = {
			small_country_region = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
		}

		effect = {
			small_country_region = {
				add_province_modifier = {
					name = "faster_integration"
					duration = 7300
				}
			}
			beepeck_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
		}
	}
	
	lorent_attack_on_beepeck = {
		icon = mission_conquer_50_development
		required_missions = { 
			lorent_halfling_suzerainty
			lorent_dameshead_ambitions 
		}
		position = 4
		
		provinces_to_highlight = {
			area = beepeck_area
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		
		trigger = {
			beepeck_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		
		effect = {
			add_country_modifier = {
				name = "imperial_ambition"
				duration = 5475
			}
		}
	}
}


lorent_imperial_expansion = {
	slot = 5
	generic = no
	ai = yes
	potential = {
		tag = A01
	}
	has_country_shield = yes
	
	lorent_our_wexonard_emperor = {
		icon = mission_empire
		required_missions = { }
		position = 1
		
		trigger = {
			A30 = {
				is_emperor = yes
			}
		}
		
		effect = {
			upper_winebay_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			lower_winebay_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			eastern_winebay_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			add_country_modifier = {
				name = "lorent_emperor_disregards_lorentish_advances"	#Its called Merchant Navy in-game
				duration = 5475
			}
		}
	}
	
	lorent_secure_the_wine_trade = {
		icon = mission_galleys_in_port
		required_missions = { lorent_our_wexonard_emperor }
		#position = 2
		
		provinces_to_highlight = {
			OR = {
				area = upper_winebay_area
				area = lower_winebay_area
				area = eastern_winebay_area
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		
		trigger = {
			home_trade_node = {
				is_strongest_trade_power = ROOT
			}
		}

		effect = {
			add_country_modifier = {
				name = "eng_channel_dominance"	#Its called Merchant Navy in-game
				duration = 5475
			}
			
			hidden_effect = {
				random_active_trade_node = {
					limit = {
						any_trade_node_member_province = {
							is_capital = yes
							owned_by = ROOT
						}
					}
					capital_scope = {
						save_event_target_as = reward_province
					}
					random_trade_node_member_province = {
						limit = {
							owned_by = ROOT
							development = CAPITAL
							is_capital = no
						}
						save_event_target_as = reward_province
					}
					random_trade_node_member_province = {
						limit = {
							owned_by = ROOT
							development = CAPITAL
							is_capital = no
							development = 20
						}
						save_event_target_as = reward_province
					}
					random_trade_node_member_province = {
						limit = {
							owned_by = ROOT
							development = CAPITAL
							is_capital = no
							development = 25
						}
						save_event_target_as = reward_province
					}
					random_trade_node_member_province = {
						limit = {
							owned_by = ROOT
							development = CAPITAL
							is_capital = no
							development = 30
						}
						save_event_target_as = reward_province
					}
				}
			}
			if = {
				limit = { has_saved_event_target = reward_province }
				event_target:reward_province = {
					add_province_modifier = {
						name = "dominant_trade_hub"
						duration = -1
					}
				}
			}
			else = {
				custom_tooltip = dominant_home_node_reward_tooltip
			}
		}
	}
	
	lorent_dameshead_ambitions = {
		icon = mission_rb_a_mighty_fleet
		required_missions = { lorent_secure_the_wine_trade }
		position = 3
		
		trigger = {
			num_of_transport = 10
			num_of_galley = 30
		}
		
		effect = {
			add_country_modifier = {
				name = "lorent_lorentish_naval_drills"
				duration = 7300
			}
			west_dameshead_region = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
		}
	}
	
	lorent_conquer_west_dameshead = {
		icon = mission_assemble_an_army
		required_missions = { lorent_dameshead_ambitions }
		position = 4
		
		provinces_to_highlight = {
			region = west_dameshead_region
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		
		trigger = {
			west_dameshead_region = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		
		effect = {
			add_country_modifier = {
				name = "generic_into_anbennar"
				duration = 7300
			}
			west_damesear_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			east_damesear_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
		}
	}
	
	lorent_invasion_of_damesear = {
		icon = mission_unite_home_region
		required_missions = { lorent_conquer_west_dameshead }
		#position = 5
		
		provinces_to_highlight = {
			OR = {
				area = west_damesear_area
				area = east_damesear_area
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		
		trigger = {
			west_damesear_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			east_damesear_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		
		effect = {
			add_country_modifier = {
				name = "army_enthusiasm"
				duration = 7300
			}
			damerian_dales_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			silverwoods_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			eastneck_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
		}
	}
	
	lorent_control_the_electors = {
		icon = mission_rb_control_electors
		required_missions = { lorent_invasion_of_damesear }
		#position = 6
		
		trigger = {
			if = {
				limit = {
					num_of_electors = 3
				}
				OR = {
					calc_true_if = {
						amount = 3
						all_elector = {
							preferred_emperor = ROOT
						}
					}
					calc_true_if = {
						amount = 3
						all_elector = {
							vassal_of = ROOT
						}
					}
				}
				else = {
					always = yes
				}
			}
		}
		
		effect = {
			add_country_modifier = {
				name = "eng_rb_hre_relations"
				duration = 5475
			}
		}
	}
	
	lorent_anbennar_emperor = {
		icon = mission_rb_war_of_the_roses
		required_missions = { lorent_control_the_electors }
		#position = 7
		
		trigger = {
			OR = {
				ROOT = {
					is_emperor = yes
					hre_reform_level = 6
				}
				AND = {
					NOT = { hre_size = 1 }
					NOT = { exists = Z01 }	#Anbennar
				}
			}
		}
		
		effect = {
			add_country_modifier = {
				name = "eng_rb_imperial_ban_mod"
				duration = 7300
			}
		}
	
	}
}